from flask import Flask, request, render_template
from .botcatch import BotCatch
app = Flask(__name__)


# Responde a requisição padrão
@app.route('/')
def home():
    
    return render_template('home.html')


# Responde a requisição de analise por usuário
@app.route('/analise/',  methods=['POST'])
def analise():
    user = request.form['user']

    botcatch = BotCatch()

    # Analisando usuário
    (prob_bot, tweets) = botcatch.user(user)

    # rules
    # TODO
    rules = []
    return render_template('analise.html', user=user, tweets=enumerate(tweets[:6]), prob_bot=prob_bot, rules=rules)

# Detalha as funcionalidades do App
@app.route('/sobre')
def sobre():
    
    return render_template('sobre.html')

# Running
if __name__ == "__main__":
    app.run(debug=True, use_reloader=True)