"""
Responsavel por preprocessar e analisar contas do Twitter
"""
import tweepy           # Analisar conta do Twitter
import numpy as np
from sklearn.externals import joblib # Importar modelo treinado


# Configurações usadas
MODEL_PATH =  './models/gradient_boosting_model.sav'
TWITTER_AUTH_FILE = './twitter/auth.txt'
MAX_TWEETS = 10
X_MEAN = np.array([7.82910189e-04, 8.21390672e+02, 6.70964993e+02, 6.44379488e+02,
                   6.29929713e+00, 4.19560424e-01, 2.08397972e-01, 1.92822741e-01,
                   1.45938137e+02, 2.19441887e-01, 8.04496141e-01, 1.05354669e+01])
X_STD = np.array([2.79695771e-02, 1.26602590e+04, 4.01588583e+03, 1.86506585e+03,
                  9.98180659e+00, 3.61762787e-01, 3.05248340e-01, 2.93106768e-01,
                  1.05660318e+03, 1.04202646e+00, 4.58956597e+00, 7.49227776e+01])


# Classe para analise de contas no Twitter
class BotCatch:

    def __init__(self):
        
        self.clf = joblib.load(MODEL_PATH)
        self.api = self.__api()

    def __api(self):
        """
        Autenticando com o Twitter
        """
        # Autenticando com o twitter
        with open(TWITTER_AUTH_FILE) as authfile:
            line = authfile.readline()
            [c_key, c_secret, a_token, a_token_secret] = line.split()

        # Autenticando Tweetpy
        auth = tweepy.OAuthHandler(c_key, c_secret)
        auth.set_access_token(a_token, a_token_secret)

        api = tweepy.API(auth)

        return api

    def api_status():
        """
        Retornar o número de requisições disponiveis
        """
        pass        

    def user(self, user_name):
        """
        Obtem os dados do usuário
        """
        
        # Receber dados do Twitter
        user = self.api.get_user(user_name)
        
        user_data = np.zeros(12)

        # Dados do usuários
        user_data[0] = user.verified
        user_data[1] = user.followers_count
        user_data[2] = user.favourites_count
        user_data[3] = user.friends_count
        user_data[4] = user.friends_count/user.followers_count

        # Dados dos tweets dos usuários
        tweets = []
        timeline = user.timeline()
        sample_size = len(timeline)

        for post in timeline:
            tweets.append(post.text)

            user_data[5] += len(post.entities['user_mentions'])/sample_size
            user_data[6] += len(post.entities['hashtags'])/sample_size
            user_data[7] += len(post.entities['urls'])/sample_size
            user_data[8] += post.retweet_count/sample_size
            user_data[9] += post.favorite_count/sample_size

        # centralizar valores
        user_data = (user_data - X_MEAN) / X_STD
        # reshape para classificar o usuário

        user_data[11] = 0
        user_data[11] = 0

        user_data = user_data.reshape(1,-1)


        prob = self.clf.predict_proba(user_data)[0][1]
        # classificar
        return (prob, tweets)